import axiosClient from "../api/axiosClient";

const movieAPI = {
  getList: (data) => {
    const url = `/movie/popular?api_key=${process.env.REACT_APP_KEY_API}` ;
    const params = {
        page: data.page,
    }
    return axiosClient.get(url, {params});
  },
};

export default movieAPI;