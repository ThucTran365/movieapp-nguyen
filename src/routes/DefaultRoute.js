import { Route } from 'react-router-dom';
import Layout from '../layouts/index';

const Index = ({ children, location, layout, ...rest }) => {
  const LayoutTemplate = Layout(layout);
  return (
    <Route
      {...rest}
      render={() => <LayoutTemplate>{children}</LayoutTemplate>}>
    </Route>
  );
};

export default Index;