import { BrowserRouter as Router, Switch } from 'react-router-dom';
import DefaultRoute from './DefaultRoute';
import Movies from '../features/movie/Movies';

const Routes = () => ( 
  <Router>
    <Switch>
      <DefaultRoute layout="auth" exact path="/" >
            <Movies />
      </DefaultRoute>
    </Switch>
  </Router>

)

 export default Routes;