import { all } from 'redux-saga/effects';
import movies from "../store/movie/movieSaga";

export function* rootSaga(){
    yield all([...movies]);
}
