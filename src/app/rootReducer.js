import { combineReducers } from "@reduxjs/toolkit";
import movies from "../store/movie/movieSlice";

const reducer = combineReducers({
  movies,
});

export default reducer;