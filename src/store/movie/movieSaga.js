import movieAPI from "../../services/movieAPI";
import { call, put, takeLatest, delay } from "redux-saga/effects";
import { getListMoviesFail, getListMoviesRequest, getListMoviesSuccess } from "./movieSlice";

function* getListMovies(action) {
  try {
    const dataResult = yield call(movieAPI.getList, action.payload);
    yield delay(500);
    yield put(getListMoviesSuccess(dataResult.results));
  } catch (error) {
    yield put(getListMoviesFail(error));
  }
}
function* watchGetList() {
  yield takeLatest(getListMoviesRequest, getListMovies);
}

const saga = [watchGetList()];

export default saga;