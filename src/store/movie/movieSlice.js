import { createSlice } from "@reduxjs/toolkit";
import { DEFAULT_STATE } from "../../constants/store";

const initialState: State = {
    ...DEFAULT_STATE,
    list: [],
};

export const movieSlice = createSlice({
    name: 'movies',
    initialState,
    reducers: {
        getListMoviesRequest: (state) => {
            state.loading = true;
        },
        getListMoviesSuccess: (state, action) => {
            state.loading = false;
            let listMovies = state.list.concat(action.payload);
            state.list = listMovies;
            state.status = true;
        },
        getListMoviesFail: (state, action) => {
            state.error = action.payload;
            state.status = false;
        },
    },
});

export const {
    getListMoviesRequest,
    getListMoviesSuccess,
    getListMoviesFail
} = movieSlice.actions;

export default movieSlice.reducer;