import CardMovie from './components/cardMovie';
import Loading from './Loading/Loading';
import { useSelector, useDispatch } from 'react-redux';
import { getListMoviesRequest } from '../../../store/movie/movieSlice';
import React, { useEffect } from 'react';

const Index = () => {
  const [list, loading] = useSelector(({ movies: { list, loading } }) => [
    list,
    loading
  ]);

  let numPage = 2;
  const dispatch = useDispatch();

  useEffect(() => {
    dispatch(getListMoviesRequest({ page: 1 }));
  }, [dispatch]);

  useEffect(() => {
    const footer = document.querySelector('#footer');
    const listItem = document.querySelector('#listItem');
    window.addEventListener('scroll', () => {
      if (
        window.scrollY + window.innerHeight >=
        listItem.clientHeight + listItem.offsetTop + footer.clientHeight - 700
      ) {
        dispatch(getListMoviesRequest({
          page: numPage++,
        }));
      }
    });
    return () => {
      window.removeEventListener('scroll', Event)
    }
  }, [dispatch, numPage]);

  return (
    <>
      <div id="listItem"
        style={{
          display: 'flex',
          justifyContent: 'center',
          alignItems: 'center',
          margin: '13px',
          flexWrap: 'wrap',
        }}>
        {list.map((item, key) => {
          return (
            <CardMovie
              {...item}
              key={key}
            />
          )
        })}
      </div>
      {loading ? <Loading /> : ""}
    </>
  );
}

export default Index;