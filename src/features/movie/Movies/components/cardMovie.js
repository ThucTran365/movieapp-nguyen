import styles from './style.module.css';
import dayjs from "dayjs";
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faEllipsis } from '@fortawesome/free-solid-svg-icons';
import { faPercent } from '@fortawesome/free-solid-svg-icons';

const CardMovie = ({ title, vote_average, release_date, poster_path }) => {

    const API_IMG = "https://www.themoviedb.org/t/p/w220_and_h330_face";
    return (
        <div className={styles.card_movie}>
            <div className={styles.card_movie_content}>
                <div className={styles.card_movie_img}>
                    <div className={styles.img}>
                        <img src={API_IMG + poster_path} alt="img card movie" />
                    </div>
                    <div className={styles.card_movie_percent}>
                        <div className={styles.percent}>{vote_average * 10}
                            <FontAwesomeIcon className={styles.icons} icon={faPercent} />
                        </div>
                    </div>
                    <div className={styles.card_movie_option}>
                        <div className={styles.option}>
                            <FontAwesomeIcon icon={faEllipsis} />
                        </div>
                    </div>
                </div>
                <div className={styles.card_movie_content}>
                    <div className={styles.items}>{title}</div>
                    <div className={styles.day}>{`${dayjs(`${release_date}`).format("MMM D, YYYY")}`}</div>
                </div>
            </div>
        </div>
    );
}

export default CardMovie;