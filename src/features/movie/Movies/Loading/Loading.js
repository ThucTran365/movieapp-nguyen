import icons from '../../../../assets/images/480px-Loader.gif';
import loading from './loading.module.css';

const Loading = (props) => {
  return (
    <div className={loading.content}>
      <img  src={icons} alt="icons loading" />
    </div>
  )
}

export default Loading;